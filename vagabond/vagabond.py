import pkgutil
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class Vagabond:
    def __init__(self, database_url):
        self.engine = create_engine(database_url)
        # create a configured "Session" class
        session = sessionmaker(bind=self.engine)

        # create a Session
        self.db = session()

    def walk_files(self, package):
        modules = []
        prefix = package.__name__ + "."
        for importer, modname, ispkg in pkgutil.iter_modules(package.__path__, prefix):
            mod = __import__(modname, fromlist="dummy")
            if ispkg:
                modules.extend(self.walk_files(mod))
            else:
                modules.append(mod)
        return modules

    @staticmethod
    def generate_ordered_db_objects(object_list):
        ddl_list = []
        for obj in object_list:
            if hasattr(obj, 'DbObject'):
                ddl_list.extend(x for x in obj.DbObject.walk_depends() if x not in ddl_list)
        return ddl_list

    def create_objects(self, package):
        objs = self.walk_files(package)
        for obj in self.generate_ordered_db_objects(objs):
            self.db.execute(obj.UPGRADE)
    
    def drop_objects(self, package):
        objs = self.walk_files(package)
        for obj in reversed(self.generate_ordered_db_objects(objs)):
            self.db.execute(obj.DOWNGRADE)
