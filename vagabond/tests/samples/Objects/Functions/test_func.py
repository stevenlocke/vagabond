from vagabond import DatabaseObject


class DbObject(DatabaseObject):
    UPGRADE = """
    create or replace function test_func(integer) RETURNS integer
    AS 'select $1 + 20;'
    LANGUAGE SQL
    IMMUTABLE
    RETURNS NULL ON NULL INPUT;
    """

    DOWNGRADE = """
    drop function if exists public.test_func(integer)
    """

    DEPENDS = []
