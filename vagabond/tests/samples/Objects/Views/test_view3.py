from vagabond.tests.samples.Objects.Functions import test_func
from vagabond.tests.samples.Objects.Views import test_view2

from vagabond import DatabaseObject
from vagabond.tests.samples.Objects.Materialized_Views import test_matview


class DbObject(DatabaseObject):
    UPGRADE = """
    create or replace view public.test_view3 as
    select
      public.test_func(tst) as tst,
      m.*
    from public.test_view2 t
    inner join public.test_matview m
      on 1=1
    """

    DOWNGRADE = """
    drop view if exists public.test_view3
    """

    DEPENDS = [test_view2, test_func, test_matview]
