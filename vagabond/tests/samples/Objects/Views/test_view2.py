from vagabond import DatabaseObject
from vagabond.tests.samples.Objects.Views import test_view


class DbObject(DatabaseObject):
    UPGRADE = """
    create or replace view public.test_view2 as
    select tst from public.test_view
    """

    DOWNGRADE = """
    drop view if exists public.test_view2
    """

    DEPENDS = [test_view]
