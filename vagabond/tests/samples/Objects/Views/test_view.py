from vagabond import DatabaseObject
from vagabond.tests.samples.Objects.Functions import test_func


class DbObject(DatabaseObject):
    UPGRADE = """
    create or replace view public.test_view as
    select public.test_func(1) as tst
    """

    DOWNGRADE = """
    drop view if exists public.test_view
    """

    DEPENDS = [test_func]
