import unittest

import testing.postgresql as pg

from vagabond.tests.samples import Objects
from vagabond.vagabond import Vagabond
from vagabond.db_object import DatabaseObject


class VagabondDbTests(unittest.TestCase):
    def setUp(self):
        self.postgresql = pg.Postgresql()
        self.wanderer = Vagabond(self.postgresql.url())

    def tearDown(self):
        self.wanderer.db.rollback()
        self.postgresql.stop()

    def test_create_drop(self):
        self.wanderer.create_objects(Objects)
        self.wanderer.drop_objects(Objects)

    def test_generate_ordered_db_objects(self):
        pass
