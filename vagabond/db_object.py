from abc import ABC


class DatabaseObject(ABC):
    DEPENDS = []
    UPGRADE = ""
    DOWNGRADE = ""

    def __init__(self, depends, upgrade, downgrade):
        self.DEPENDS = depends
        self.UPGRADE = upgrade
        self.DOWNGRADE = downgrade

    @classmethod
    def walk_depends(cls):
        dep_tree = []
        for deps in cls.DEPENDS:
            dep_tree.extend(deps.DbObject.walk_depends())
        dep_tree.append(cls)
        return dep_tree
